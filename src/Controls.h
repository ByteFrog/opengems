/*
 * Controls.h
 *
 *  Created on: Jul 20, 2018
 *      Author: Aelia Virdaeus
 */

#pragma once

#include <SDL2/SDL.h>

class Controls {
public:
	enum Button { LEFT = 0x1, RIGHT = 0x2, DOWN = 0x4, SWITCH = 0x8, PAUSE = 0x10 };

	Controls();
	virtual ~Controls();
	void Update(const Uint8* keyboardState);
	bool Pressed(Button btn);
	bool Released (Button btn);
	bool Held(Button btn);
	bool Down(Button btn);

private:
	unsigned char curButtons;
	unsigned char preButtons;
};
