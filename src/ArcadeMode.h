/*
 * ArcadeMode.h
 *
 *  Created on: Jul 18, 2018
 *      Author: Aelia Virdaeus
 */

#pragma once

#include "GameState.h"
#include "Game.h"
#include "utils2d.h"
#include <random>
#include <list>
//#include <vector>

enum class BlockType { NONE, DELETING, RED, PURPLE, BLUE, GREEN, YELLOW, ORANGE };
enum class PlayerState { CREATE, MOVE, SCAN, DELETE, SHIFT, GAMEOVER };

class ArcadeMode: public GameState {
public:
	ArcadeMode(Game* game);
	~ArcadeMode();
	void Initialize();
	void Terminate();

	void Pause();
	void Resume();

	void Update();
	void Render(SDL_Renderer* renderer);

	void Reset();

private:
	void CreatePiece();
	void MovePiece();
	void ScanLines();
	void DeleteBlocks();
	void ShiftBlocks();
	void EndGame();

	void DrawBlock(BlockType block, int x, int y, SDL_Renderer* renderer);
	void PrintBlock(BlockType block);
	void DrawNumber(unsigned int num, int x, int y, SDL_Renderer* renderer);
	BlockType GenerateBlock();

	BlockType field[6][13];
	BlockType curPiece[3];
	BlockType nextPiece[3];
	PlayerState state;
	std::default_random_engine randEngine;
	std::uniform_int_distribution<int> blockDistr;
	//This position refers to the location of the bottom-most block for the current piece. It's
	//vertically scaled up by 2 to allow for half-spaces (eg. y = 3 means 1.5 blocks from the top.)
	ivec2 piecePos;
	unsigned int score;
	unsigned int combo;
	unsigned int level;
	unsigned int blocks;
	int curTicks;
	int ticksPerDrop;
	bool pieceLanded;
	bool deletedVisible;
	bool gameOverVis;
	bool pause;

	const int BLOCK_SIZE;
	const int BLOCK_SPACING;
	const int INIT_DROP_TICKS;	//How many ticks to move a piece down at level 0.
	const int TICKS_PER_LEVEL;	//How many ticks to remove from the drop rate on level up.
	const int TICKS_PER_LOCK;	//How many ticks until a piece locks in place.
	const int BLINK_TICKS;		//How many ticks to blink deleted gems.
	const int TICKS_PER_BLINK;	//How many ticks a deleted gem should be visible/invisible for.
	const unsigned int BASE_SCORE;			//How many points a row of 3 gems is worth.
	const unsigned int BLOCKS_PER_LEVEL;	//How many blocks must be removed to increase level.
	const ivec2 FIELD_POS;
	const ivec2 PREVIEW_POS;
	const ivec2 SCORE_POS;
	const ivec2 LEVEL_POS;
	const ivec2 BLOCKS_POS;
	const ivec2 GAME_OVER_POS;
	const ivec2 PAUSE_POS;
};
