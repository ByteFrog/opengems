/*
 * main.cpp
 *
 *  Created on: Jul 16, 2018
 *      Author: Aelia Virdaeus
 */

#include "Game.h"

int main(int argc, char** argv) {
	Game game;
	if(game.Initialize()) {
		game.Run();
	}
}
