/*
 * ArcadeMode.cpp
 *
 *  Created on: Jul 18, 2018
 *      Author: Aelia Virdaeus
 */

#include "ArcadeMode.h"

ArcadeMode::ArcadeMode(Game* game)
: GameState(game),
  state(PlayerState::CREATE),
  blockDistr(1,6),
  curTicks(0),
  score(0),
  combo(1),
  level(0),
  blocks(0),
  ticksPerDrop(20),
  pieceLanded(false),
  deletedVisible(true),
  gameOverVis(false),
  pause(false),
  BLOCK_SIZE(32),
  BLOCK_SPACING(0),
  INIT_DROP_TICKS(20),
  TICKS_PER_LEVEL(2),
  TICKS_PER_LOCK(20),
  BLINK_TICKS(15),
  TICKS_PER_BLINK(3),
  BASE_SCORE(3),
  BLOCKS_PER_LEVEL(35),
  FIELD_POS(32,32),
  PREVIEW_POS(256,32),
  SCORE_POS(240,240),
  LEVEL_POS(240,304),
  BLOCKS_POS(240,368),
  GAME_OVER_POS(64,158),
  PAUSE_POS(80,170)
{ }

ArcadeMode::~ArcadeMode() {
}

void ArcadeMode::Initialize() {
	randEngine.seed(SDL_GetTicks());

	Reset();
}

void ArcadeMode::Terminate() {
}

void ArcadeMode::Pause() {
}

void ArcadeMode::Resume() {
}

void ArcadeMode::Update() {

	if(game->controls.Pressed(Controls::PAUSE) && state != PlayerState::GAMEOVER) {
		pause = !pause;
	}

	if(!pause) {
		switch(state) {
		case PlayerState::CREATE:
			CreatePiece();
			break;
		case PlayerState::MOVE:
			MovePiece();
			break;
		case PlayerState::SCAN:
			ScanLines();
			break;
		case PlayerState::DELETE:
			DeleteBlocks();
			break;
		case PlayerState::SHIFT:
			ShiftBlocks();
			break;
		case PlayerState::GAMEOVER:
			EndGame();
			break;
		default:
			break;
		}
	}
}

void ArcadeMode::Render(SDL_Renderer* renderer) {
	BlockType curBlock;
	int x = 0, y = 0;
	SDL_Rect srcRect, dstRect;
	//Clear the screen
	SDL_SetRenderDrawColor(renderer, 0x00, 0x00, 0x00, 0xFF);
	SDL_RenderClear(renderer);

	//Draw the field
	srcRect.x = 0;
	srcRect.y = 0;
	srcRect.w = 160;
	srcRect.h = 240;
	dstRect.x = 0;
	dstRect.y = 0;
	dstRect.w = SCREEN_WIDTH;
	dstRect.h = SCREEN_HEIGHT;
	SDL_RenderCopy(renderer, game->txField, &srcRect, &dstRect);

	//Draw the displays
	DrawNumber(score, SCORE_POS.x, SCORE_POS.y, renderer);
	DrawNumber(level, LEVEL_POS.x, LEVEL_POS.y, renderer);
	DrawNumber(blocks, BLOCKS_POS.x, BLOCKS_POS.y, renderer);

	if(!pause) {

		//Draw the locked blocks.
		for(int iy = 0; iy < 13; iy++) {
			for(int ix = 0; ix < 6; ix++) {
				curBlock = field[ix][iy];
				if(curBlock != BlockType::NONE) {
					x = FIELD_POS.x + ((BLOCK_SIZE + BLOCK_SPACING) * ix) + BLOCK_SPACING;
					y = FIELD_POS.y + ((BLOCK_SIZE + BLOCK_SPACING) * iy) + BLOCK_SPACING;
					DrawBlock(curBlock, x, y, renderer);
				}
			}
		}

		//Draw the preview
		for(int i = 0; i < 3; i++) {
			x = PREVIEW_POS.x + BLOCK_SPACING;
			y = PREVIEW_POS.y + ((BLOCK_SIZE + BLOCK_SPACING) * i) + BLOCK_SPACING;
			DrawBlock(nextPiece[i], x, y, renderer);
		}

		//Draw the current piece and top bar
		if(state == PlayerState::MOVE) {
			x = FIELD_POS.x + ((BLOCK_SIZE + BLOCK_SPACING) * piecePos.x) + BLOCK_SPACING;
			for(int i = 0; i < 3; ++i) {
				y = FIELD_POS.y + (((BLOCK_SIZE + BLOCK_SPACING) / 2) * (piecePos.y - 4 + (i * 2))) + BLOCK_SPACING;
				DrawBlock(curPiece[i], x, y, renderer);
			}
			srcRect.x = 0;
			srcRect.y = 0;
			srcRect.h = srcRect.w = 16;
			dstRect.y = 0;
			dstRect.h = dstRect.w = BLOCK_SIZE;
			for(int i = 1; i < 7; ++i) {
				dstRect.x = (FIELD_POS.x + BLOCK_SPACING) * i;
				SDL_RenderCopy(renderer, game->txField, &srcRect, &dstRect);
			}
		}
	}

	if(pause) {
		srcRect.x = 0;
		srcRect.y = 64;
		srcRect.w = 48;
		srcRect.h = 8;
		dstRect.x = PAUSE_POS.x;
		dstRect.y = PAUSE_POS.y;
		dstRect.w = 96;
		dstRect.h = 16;
		SDL_RenderCopy(renderer, game->txAssets, &srcRect, &dstRect);
	}

	//Draw the Game Over display, if required.
	if(gameOverVis) {
		srcRect.x = 0;
		srcRect.y = 32;
		srcRect.w = 64;
		srcRect.h = 32;
		dstRect.x = GAME_OVER_POS.x;
		dstRect.y = GAME_OVER_POS.y;
		dstRect.w = 128;
		dstRect.h = 64;
		SDL_RenderCopy(renderer, game->txAssets, &srcRect, &dstRect);
	}

	SDL_RenderPresent(renderer);
}

void ArcadeMode::Reset() {

	state = PlayerState::CREATE;
	curTicks = 0;
	score = 0;
	combo = 1;
	level = 0;
	blocks = 0;
	ticksPerDrop = INIT_DROP_TICKS;
	pieceLanded = false;
	deletedVisible = true;
	gameOverVis = false;

	for(int ix = 0; ix < 6; ix++) {
		for(int iy = 0; iy < 13; iy++) {
			field[ix][iy] = BlockType::NONE;
		}
	}

	for(int i = 0; i < 3; i++) {
		nextPiece[i] = GenerateBlock();
	}

	//Pick a random song to play.
	if(Mix_PlayingMusic() != 0) {
		Mix_HaltMusic();
	}

	std::uniform_int_distribution<int> randSong(1,3);
	switch(randSong(randEngine)) {
	case 1:
		Mix_PlayMusic(game->musGame01, -1);
		break;
	case 2:
		Mix_PlayMusic(game->musGame02, -1);
		break;
	case 3:
		Mix_PlayMusic(game->musGame03, -1);
		break;
	}

}

void ArcadeMode::DrawBlock(BlockType block, int x, int y, SDL_Renderer* renderer) {
	SDL_Rect srcRect, dstRect;
	dstRect.x = x;
	dstRect.y = y;
	dstRect.w = dstRect.h = BLOCK_SIZE;
	srcRect.w = srcRect.h = 16;
	switch(block) {
	case BlockType::RED:
		srcRect.x = 0;
		srcRect.y = 0;
		SDL_RenderCopy(renderer, game->txAssets, &srcRect, &dstRect);
		break;
	case BlockType::ORANGE:
		srcRect.x = 16;
		srcRect.y = 0;
		SDL_RenderCopy(renderer, game->txAssets, &srcRect, &dstRect);
		break;
	case BlockType::YELLOW:
		srcRect.x = 32;
		srcRect.y = 0;
		SDL_RenderCopy(renderer, game->txAssets, &srcRect, &dstRect);
		break;
	case BlockType::GREEN:
		srcRect.x = 48;
		srcRect.y = 0;
		SDL_RenderCopy(renderer, game->txAssets, &srcRect, &dstRect);
		break;
	case BlockType::BLUE:
		srcRect.x = 0;
		srcRect.y = 16;
		SDL_RenderCopy(renderer, game->txAssets, &srcRect, &dstRect);
		break;
	case BlockType::PURPLE:
		srcRect.x = 16;
		srcRect.y = 16;
		SDL_RenderCopy(renderer, game->txAssets, &srcRect, &dstRect);
		break;
	case BlockType::DELETING:
		if(deletedVisible) {
			SDL_SetRenderDrawColor(renderer, 0xFF, 0xFF, 0xFF, 0xFF);
			SDL_RenderFillRect(renderer, &dstRect);
		}
		break;
	default:
		break;
	}
}

void ArcadeMode::CreatePiece() {
	combo = 1;
	if(field[3][0] == BlockType::NONE) {
		for(int i = 0; i < 3; i++) {
			curPiece[i] = nextPiece[i];
			nextPiece[i] = GenerateBlock();
		}
		piecePos.x = 3;
		piecePos.y = 0;
		state = PlayerState::MOVE;
	}
	else {
		state = PlayerState::GAMEOVER;
	}

}

void ArcadeMode::MovePiece() {
	if(game->controls.Pressed(Controls::LEFT) && piecePos.x > 0) {
		if(field[piecePos.x - 1][(piecePos.y + 1) / 2] == BlockType::NONE) {
			piecePos.x -= 1;
		}
	}
	if(game->controls.Pressed(Controls::RIGHT) && piecePos.x < 5) {
		if(field[piecePos.x + 1][(piecePos.y + 1) / 2] == BlockType::NONE) {
			piecePos.x += 1;
		}
	}
	if(game->controls.Down(Controls::DOWN)) {
		curTicks = ticksPerDrop + TICKS_PER_LOCK;
	}
	if(game->controls.Pressed(Controls::SWITCH)) {
		BlockType temp = curPiece[2];
		curPiece[2] = curPiece[1];
		curPiece[1] = curPiece[0];
		curPiece[0] = temp;
		Mix_PlayChannel(-1, game->sndSwitch, 0);
	}
	if(curTicks >= ticksPerDrop + TICKS_PER_LOCK && pieceLanded) {
		if(piecePos.y/2 < 2) {
			Mix_HaltMusic();
			Mix_PlayMusic(game->musGameOver, -1);
			state = PlayerState::GAMEOVER;
		}
		else {
			for(int i = 0; i < 3; i++) {
				field[piecePos.x][(piecePos.y/2)-2+i] = curPiece[i];
				curTicks = 0;
				pieceLanded = false;
				Mix_PlayChannel(-1, game->sndLand, 0);
				state = PlayerState::SCAN;
			}
		}
	}
	if(curTicks++ >= ticksPerDrop) {
		pieceLanded = true;
		if(piecePos.y < 24) {
			if(field[piecePos.x][(piecePos.y + 2) / 2] == BlockType::NONE) {
				piecePos.y += 1;
				curTicks = 0;
				pieceLanded = false;
			}
		}
	}

}

void ArcadeMode::ScanLines() {
	/* The algorithm here allows for the same gem to be added to the deletion list multiple times.
	 * It could be optimized, but given that duplicates don't interfere with the deletion algorithm
	 * and the number of duplicates is relatively small (so there's little additional memory cost)
	 * it didn't seem worth the added processing to check for duplicates.
	 */

	BlockType prevBlock, curBlock;
	int ix = 0, iy = 0;
	int count = 1;
	int points = 0;
	std::vector<ivec2> deletions;

	//Scan vertically
	for(ix = 0; ix < 6; ix++) {
		curBlock = field[ix][12];
		count = 1;
		for(iy = 11; iy >= 0 && curBlock != BlockType::NONE; iy--) {
			prevBlock = curBlock;
			curBlock = field[ix][iy];
			if(curBlock == prevBlock) {
				count++;
				if(count == 3) {
					deletions.push_back(ivec2(ix,iy+2));
					deletions.push_back(ivec2(ix,iy+1));;
				}
				if(count >= 3) {
					deletions.push_back(ivec2(ix,iy));
					points += 1;
				}
			}
			else {
				count = 1;
			}
		}
	}

	//Scan horizontally
	for(iy = 0; iy < 13; iy++) {
		curBlock = field[0][iy];
		count = 1;
		for(ix = 1; ix < 6; ix++) {
			prevBlock = curBlock;
			curBlock = field[ix][iy];
			if(curBlock != BlockType::NONE && curBlock == prevBlock) {
				count++;
				if(count == 3) {
					deletions.push_back(ivec2(ix-2,iy));
					deletions.push_back(ivec2(ix-1,iy));
				}
				if(count >= 3) {
					deletions.push_back(ivec2(ix,iy));
					points += 1;
				}
			}
			else {
				count = 1;
			}
		}
	}

	//Scan diagonally (top left to bottom right)
	for(int iStart = -3; iStart < 11; iStart++) {
		if(iStart < 0) {
			ix = iStart * -1;
			iy = 0;
		}
		else {
			ix = 0;
			iy = iStart;
		}
		curBlock = BlockType::NONE;
		count = 0;
		while(ix < 6 && iy < 13) {
			prevBlock = curBlock;
			curBlock = field[ix][iy];
			if(curBlock != BlockType::NONE && curBlock == prevBlock) {
				count++;
				if(count == 3) {
					deletions.push_back(ivec2(ix-2,iy-2));
					deletions.push_back(ivec2(ix-1,iy-1));
				}
				if(count >= 3) {
					deletions.push_back(ivec2(ix,iy));
					points += 1;
				}
			}
			else {
				count = 1;
			}
			ix++;
			iy++;
		}
	}

	//Scan diagonally (top right to bottom left)
	for(int iStart = -3; iStart < 11; iStart++) {
		if(iStart < 0) {
			ix = iStart + 5;
			iy = 0;
		}
		else {
			ix = 5;
			iy = iStart;
		}
		curBlock = BlockType::NONE;
		count = 0;
		while(ix >=0 && iy < 13) {
			prevBlock = curBlock;
			curBlock = field[ix][iy];
			if(curBlock != BlockType::NONE && curBlock == prevBlock) {
				count++;
				if(count == 3) {
					deletions.push_back(ivec2(ix+2,iy-2));
					deletions.push_back(ivec2(ix+1,iy-1));
				}
				if(count >= 3) {
					deletions.push_back(ivec2(ix,iy));
					points += 1;
				}
			}
			else {
				count = 1;
			}
			ix--;
			iy++;
		}
	}

	//Delete
	while(!deletions.empty()) {
		ivec2 curPos = deletions.back();
		deletions.pop_back();
		if(field[curPos.x][curPos.y] != BlockType::DELETING) {
			field[curPos.x][curPos.y] = BlockType::DELETING;
			blocks += 1;
		}
	}


	if(points > 0) {
		score += points * combo * (1 + level) * BASE_SCORE;
		++combo;
		if(blocks >= (1 + level) * BLOCKS_PER_LEVEL && level < 9) {
			++level;
			ticksPerDrop -= TICKS_PER_LEVEL;
		}
		Mix_PlayChannel(-1, game->sndVanish, 0);
		state = PlayerState::DELETE;
	}
	else {
		state = PlayerState::CREATE;
	}
}

void ArcadeMode::DeleteBlocks() {
	if(curTicks % TICKS_PER_BLINK == 0) {
		deletedVisible = !deletedVisible;
	}
	if(curTicks < BLINK_TICKS) {
		curTicks++;
	}
	else {
		curTicks = 0;
		for(int ix = 0; ix < 6; ix++) {
			for(int iy = 0; iy < 13; iy++) {
				if(field[ix][iy] == BlockType::DELETING) {
					field[ix][iy] = BlockType::NONE;
				}
			}
		}
		state = PlayerState::SHIFT;
	}
}

void ArcadeMode::ShiftBlocks() {
	//Got tired of trying to move blocks down by half spaces to make it look nice.
	//Scan the opposite direction so blocks only move one space per frame.
	bool blockMoved = false;
		for(int ix = 0; ix < 6; ++ix) {
			for(int iy = 11; iy >= 0; --iy) {
				if(field[ix][iy] != BlockType::NONE && field[ix][iy+1] == BlockType::NONE) {
					field[ix][iy+1] = field[ix][iy];
					field[ix][iy] = BlockType::NONE;
					blockMoved = true;
				}
			}
		}
		if(!blockMoved) {
			state = PlayerState::SCAN;
		}
}

//Only draws numbers up to 6 digits.
void ArcadeMode::DrawNumber(unsigned int num, int x, int y, SDL_Renderer* renderer) {
	if(num > 999999) {
		num = 999999;
	}
	SDL_Rect srcRect, dstRect;
	srcRect.w = srcRect.h = 6;
	dstRect.w = dstRect.h = 12;
	dstRect.y = y;
	int div = 100000;
	bool draw = false;
	for(int i = 0; i < 6; ++i) {
		dstRect.x = x + (i * 10);
		int digit = num / div;
		switch(digit) {
		case 1:
			srcRect.x = 32;
			srcRect.y = 16;
			break;
		case 2:
			srcRect.x = 37;
			srcRect.y = 16;
			break;
		case 3:
			srcRect.x = 42;
			srcRect.y = 16;
			break;
		case 4:
			srcRect.x = 47;
			srcRect.y = 16;
			break;
		case 5:
			srcRect.x = 52;
			srcRect.y = 16;
			break;
		case 6:
			srcRect.x = 32;
			srcRect.y = 22;
			break;
		case 7:
			srcRect.x = 37;
			srcRect.y = 22;
			break;
		case 8:
			srcRect.x = 42;
			srcRect.y = 22;
			break;
		case 9:
			srcRect.x = 47;
			srcRect.y = 22;
			break;
		case 0:
			srcRect.x = 52;
			srcRect.y = 22;
			break;
		default:
			srcRect.x = 52;
			srcRect.y = 22;
			break;
		}
		if(digit != 0) {
			draw = true;
		}
		if(draw || i == 5) {
			SDL_RenderCopy(renderer, game->txAssets, &srcRect, &dstRect);
		}
		num -= div * digit;
		div /= 10;
	}
}

void ArcadeMode::PrintBlock(BlockType block) {
	switch(block) {
	case BlockType::RED: printf("RED\n"); break;
	case BlockType::PURPLE: printf("PURPLE\n"); break;
	case BlockType::BLUE: printf("BLUE\n"); break;
	case BlockType::GREEN: printf("GREEN\n"); break;
	case BlockType::YELLOW: printf("YELLOW\n"); break;
	case BlockType::ORANGE: printf("ORANGE\n"); break;
	case BlockType::NONE: printf("NONE\n"); break;
	case BlockType::DELETING: printf("DELETING\n"); break;
	}
}

void ArcadeMode::EndGame() {
	bool blockFound = false;
	for(int iy = 0; iy < 13; ++iy) {
		for(int ix = 0; ix < 6; ++ix) {
			if(field[ix][iy] != BlockType::NONE) {
				blockFound = true;
				field[ix][iy] = BlockType::NONE;
			}
		}
		if(blockFound) {
			break;
		}
	}
	if(!blockFound) {
		gameOverVis = true;
		if(game->controls.Pressed(Controls::SWITCH)) {
			Reset();
		}
	}
}

BlockType ArcadeMode::GenerateBlock() {
	BlockType block;
	switch(blockDistr(randEngine)) {
	case 1: block = BlockType::RED; break;
	case 2: block = BlockType::PURPLE; break;
	case 3: block = BlockType::BLUE; break;
	case 4: block = BlockType::GREEN; break;
	case 5: block = BlockType::YELLOW; break;
	case 6: block = BlockType::ORANGE; break;
	}
	return block;
}
