/*
 * Game.h
 *
 *  Created on: Jul 16, 2018
 *      Author: Aelia Virdaeus
 */

#pragma once

#include <SDL2/SDL.h>
#include <SDL2/SDL_mixer.h>
#include <vector>
#include "Controls.h"

class GameState;

const int SCREEN_WIDTH = 320;
const int SCREEN_HEIGHT = 480;
const int TICKS_PER_FRAME = 25;

class Game {
public:
	Game();
	~Game();
	bool Initialize();
	void Run();

	Controls controls;
	SDL_Texture* txField;
	SDL_Texture* txAssets;
	Mix_Chunk* sndSwitch;
	Mix_Chunk* sndLand;
	Mix_Chunk* sndVanish;
	Mix_Music* musGame01;
	Mix_Music* musGame02;
	Mix_Music* musGame03;
	Mix_Music* musGameOver;

private:
	void PushState(GameState* state);
	void ChangeState(GameState* state);
	void PopState();

	std::vector<GameState*> stateStack;
	SDL_Window* window;
	SDL_Renderer* renderer;
};
