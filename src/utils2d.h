/*
 * utils2d.h
 *
 *  Created on: Oct. 17, 2019
 *      Author: Aelia Virdaeus
 */

#pragma once

struct ivec2 {
	ivec2(int x = 0, int y = 0) : x(x), y(y) {}
	int x;
	int y;
};
