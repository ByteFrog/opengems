/*
 * Controls.cpp
 *
 *  Created on: Jul 20, 2018
 *      Author: Aelia Virdaeus
 */

#include "Controls.h"

Controls::Controls()
: curButtons(0x00), preButtons(0x00) {
}

Controls::~Controls() {
}

void Controls::Update(const Uint8* keyboardState) {
	preButtons = curButtons;
	curButtons = 0x0;
	if(keyboardState[SDL_SCANCODE_LEFT]) {
		curButtons |= LEFT;
	}
	if(keyboardState[SDL_SCANCODE_RIGHT]) {
		curButtons |= RIGHT;
	}
	if(keyboardState[SDL_SCANCODE_DOWN]) {
		curButtons |= DOWN;
	}
	if(keyboardState[SDL_SCANCODE_Z]) {
		curButtons |= SWITCH;
	}
	if(keyboardState[SDL_SCANCODE_P]) {
		curButtons |= PAUSE;
	}
}

bool Controls::Pressed(Button btn) {
	if((curButtons & btn) != 0 && (preButtons & btn) == 0) {
		return true;
	}
	return false;
}

bool Controls::Released(Button btn) {
	if((curButtons & btn) == 0 && (preButtons & btn) != 0) {
		return true;
	}
	return false;
}

bool Controls::Held(Button btn) {
	if((curButtons & btn) != 0 && (preButtons & btn) != 0) {
		return true;
	}
	return false;
}

bool Controls::Down(Button btn) {
	if((curButtons & btn) != 0) {
		return true;
	}
	return false;
}
