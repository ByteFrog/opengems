/*
 * Game.cpp
 *
 *  Created on: Jul 16, 2018
 *      Author: Aelia Virdaeus
 */

#include "Game.h"
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdio.h>
#include "GameState.h"
#include "ArcadeMode.h"

Game::Game() {
	window = NULL;
	renderer = NULL;
	txField = NULL;
	txAssets = NULL;
	sndLand = NULL;
	sndSwitch = NULL;
	sndVanish = NULL;
}

Game::~Game() {

	//FREE ASSETS
	Mix_FreeChunk(sndLand);
	Mix_FreeChunk(sndSwitch);
	Mix_FreeChunk(sndVanish);
	Mix_FreeMusic(musGame01);
	Mix_FreeMusic(musGame02);
	Mix_FreeMusic(musGame03);
	Mix_FreeMusic(musGameOver);

	//FREE SYSTEM RESOURCES
	while(!stateStack.empty()) {
		PopState();
	}
	if(renderer != NULL) {
		SDL_DestroyRenderer(renderer);
	}
	if(window != NULL) {
		SDL_DestroyWindow(window);
	}
	Mix_Quit();
	IMG_Quit();
	SDL_Quit();
}

bool Game::Initialize() {

	if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) < 0) {
		printf("SDL failed to initialize. SDL Error: %s\n", SDL_GetError());
		return false;
	}

	window = SDL_CreateWindow("OpenGems", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
	if(window == NULL) {
		printf("SDL could not create window. SDL Error: %s\n", SDL_GetError());
		return false;
	}

	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if(renderer == NULL) {
		printf("SDL could not create renderer. SDL Error: %s\n", SDL_GetError());
		return false;
	}

	if(!(IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG)) {
		printf("SDL_image failed to initialize. SDL_image Error: %s\n", IMG_GetError());
		return false;
	}

	if(Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048) < 0) {
		printf("SDL_mixer failed to initialize. SDL_mixer Error: %s\n", Mix_GetError());
		return false;
	}

	//LOAD ASSETS

	SDL_Surface* tmpSurface = IMG_Load("assets/field.png");
	if(tmpSurface == NULL) {
		printf("Failed to load image assets/field.png SDL_image Error %s", IMG_GetError());
		return false;
	}

	txField = SDL_CreateTextureFromSurface(renderer, tmpSurface);
	if(txField == NULL) {
		printf("Failed to create texture assets/field.png SDL_image Error %s", IMG_GetError());
		return false;
	}
	SDL_FreeSurface(tmpSurface);

	tmpSurface = IMG_Load("assets/assets.png");
	if(tmpSurface == NULL) {
		printf("Failed to load image assets/assets.png SDL_image Error %s", IMG_GetError());
		return false;
	}

	txAssets = SDL_CreateTextureFromSurface(renderer, tmpSurface);
	if(txAssets == NULL) {
		printf("Failed to create texture assets/assets.png SDL_image Error %s", IMG_GetError());
		return false;
	}
	SDL_FreeSurface(tmpSurface);

	sndLand = Mix_LoadWAV("assets/NFF-stuff-up.wav");
	if(sndLand == NULL) {
		printf("Failed to load sound assets/NFF-stuff-up.wav SDL_mixer Error %s", Mix_GetError());
		return false;
	}

	sndSwitch = Mix_LoadWAV("assets/NFF-suck.wav");
	if(sndSwitch == NULL) {
		printf("Failed to load sound assets/NFF-suck.wav SDL_mixer Error %s", Mix_GetError());
		return false;
	}

	sndVanish = Mix_LoadWAV("assets/NFF-steal-02.wav");
	if(sndVanish == NULL) {
		printf("Failed to load sound assets/NFF-steal-02.wav SDL_mixer Error %s", Mix_GetError());
		return false;
	}

	musGame01 = Mix_LoadMUS("assets/POL-galactic-trek-short.wav");
	if(musGame01 == NULL) {
		printf("Failed to load music assets/POL-galactic-trek-short.wav SDL_mixer Error %s", Mix_GetError());
		return false;
	}

	musGame02 = Mix_LoadMUS("assets/POL-lone-wolf-short.wav");
	if(musGame02 == NULL) {
		printf("Failed to load music assets/POL-lone-wolf-short.wav SDL_mixer Error %s", Mix_GetError());
		return false;
	}

	musGame03 = Mix_LoadMUS("assets/POL-stealth-mode-short.wav");
	if(musGame03 == NULL) {
		printf("Failed to load music assets/POL-stealth-mode-short.wav SDL_mixer Error %s", Mix_GetError());
		return false;
	}

	musGameOver = Mix_LoadMUS("assets/POL-spirits-dance-short.wav");
	if(musGameOver == NULL) {
		printf("Failed to load music assets/POL-spirits-dance-short.wav SDL_mixer Error %s", Mix_GetError());
		return false;
	}

	return true;
}

void Game::Run() {
	bool gameRunning = true;
	SDL_Event e;
	unsigned int startTime = 0;
	GameState* state = new ArcadeMode(this);
	PushState(state);
	while(gameRunning) {

		startTime = SDL_GetTicks();
		while(SDL_PollEvent(&e) != 0) {
			if(e.type == SDL_QUIT) {
				gameRunning = false;
			}
		}
		controls.Update(SDL_GetKeyboardState(NULL));

		if(!stateStack.empty()) {
			stateStack.back()->Update();
			stateStack.back()->Render(renderer);
		}
		SDL_Delay(startTime + TICKS_PER_FRAME - SDL_GetTicks());

	}
}

void Game::PushState(GameState* state) {

	if(!stateStack.empty()) {
		stateStack.back()->Pause();
	}
	state->Initialize();
	stateStack.push_back(state);
}

void Game::ChangeState(GameState* state) {
	if(!stateStack.empty()) {
		PopState();
	}
	PushState(state);
}

void Game::PopState() {
	stateStack.back()->Terminate();
	delete stateStack.back();
	stateStack.pop_back();
	if(!stateStack.empty()) {
		stateStack.back()->Resume();
	}
}
