/*
 * GameState.h
 *
 *  Created on: Jul 16, 2018
 *      Author: Aelia Virdaeus
 */

#pragma once

#include "Game.h"

class GameState {
protected:
	GameState(Game* game) : game(game) {}
	Game* game;

public:
	virtual ~GameState() {}
	virtual void Initialize() = 0;
	virtual void Terminate() = 0;

	virtual void Pause() = 0;
	virtual void Resume() = 0;

	virtual void Update() = 0;
	virtual void Render(SDL_Renderer* renderer) = 0;
};
