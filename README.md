# OpenGems

A matching game inspired by Columns for the Sega Genesis. Written in C++ using SDL2.

# Motivation

I've dabbled in game programming for several years as a way to explore new computer sciences concepts and algorithms related to game development, but never taken a game to completion. Instead of doing pong or tetris, I thought it'd be a fun challenge to remake one of my favourite puzzle games growing up; Columns for the Sega Genesis.

# Requirements

- SDL2
- SDL2_mixer

# How to Use

There's no makefile included with this project (yet), so you'll need to compile it manually from the source code. See the requirements for which libraries you'll need.

The program can be run from the command line, or run directly from the executable. It takes no arguments.